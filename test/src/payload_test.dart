import 'dart:typed_data';

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void main() {
  final result = Uint8List.fromList([5, 78, 32]);

  test('stores the PayloadTypes and their value correctly', () {
    final writer = Payload.write()
      ..set(uint8, 5)
      ..set(uint16, 20000);

    expect(writer.length, equals(3));
    expect(binarize(writer), equals(result));
  });

  test('reads the PayloadTypes and their value correctly', () {
    final reader = Payload.read(result);

    expect(reader.length, equals(3));
    expect(reader.get(uint8), equals(5));
    expect(reader.offset, equals(1));
    expect(reader.get(uint16), equals(20000));
    expect(reader.offset, equals(3));
  });
}
