// ignore_for_file: prefer_const_constructors

import 'dart:typed_data';

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

import '../helpers/helpers.dart';

void main() {
  group('boolean', () {
    test('returns true', expectGet(boolean, [1], true));
    test('returns false', expectGet(boolean, [0], false));

    test('sets bytes to true', expectSet(boolean, true, [1]));
    test('sets bytes to false', expectSet(boolean, false, [0]));

    test('byte length is 1 for true', expectLength(boolean, true, 1));
    test('byte length is 1 for false', expectLength(boolean, false, 1));
  });

  group('Bytes', () {
    test('returns list of bytes', expectGet(Bytes(3), [1, 2, 3], [1, 2, 3]));

    test(
      'byte length is equal to list length',
      expectLength(Bytes(3), [1, 2, 3], 3),
    );
  });

  group('flags', () {
    final bytes = [5];
    final testFlags = [true, false, true, false, false, false, false, false];

    test('returns correct flags from byte', expectGet(flags, bytes, testFlags));

    test('sets byte to represent flags', expectSet(flags, testFlags, bytes));

    test('byte length is 1', expectLength(flags, testFlags, 1));

    test('throws assertion when a list of more than 8 booleans is passed', () {
      expect(
        () => flags.set([...testFlags, true], ByteData(0), 0),
        throwsAssertionMessage('Flags can only be 8 bits long'),
      );
    });
  });

  group('float32', () {
    final bytes = [63, 192, 0, 0];

    test('returns 1.5', expectGet(float32, bytes, 1.5));

    test('sets bytes to 1.5', expectSet(float32, 1.5, bytes));

    test('byte length is 4', expectLength(float32, 1.5, 4));
  });

  group('float64', () {
    final bytes = [63, 248, 0, 0, 0, 0, 0, 0];

    test('returns 1.5', expectGet(float64, bytes, 1.5));

    test('sets bytes to 1.5', expectSet(float64, 1.5, bytes));

    test('length is 8', expectLength(float64, 1.5, 8));
  });

  group('int8', () {
    test('returns -8', expectGet(int8, [248], -8));
    test('returns 8', expectGet(int8, [8], 8));

    test('sets bytes to -8', expectSet(int8, -8, [248]));
    test('sets bytes to 8', expectSet(int8, 8, [8]));

    test('byte length is 1', expectLength(int8, 8, 1));
  });

  group('int16', () {
    test('returns -16', expectGet(int16, [255, 240], -16));
    test('returns 16', expectGet(int16, [0, 16], 16));

    test('sets bytes to -16', expectSet(int16, -16, [255, 240]));
    test('sets bytes to 16', expectSet(int16, 16, [0, 16]));

    test('byte length is 2', expectLength(int16, 16, 2));
  });

  group('int32', () {
    test('returns -32', expectGet(int32, [255, 255, 255, 224], -32));
    test('returns 32', expectGet(int32, [0, 0, 0, 32], 32));

    test('sets bytes to -32', expectSet(int32, -32, [255, 255, 255, 224]));
    test('sets bytes to 32', expectSet(int32, 32, [0, 0, 0, 32]));

    test('byte length is 4', expectLength(int32, 32, 4));
  });

  group('int64', () {
    test(
      'returns -64',
      expectGet(int64, [255, 255, 255, 255, 255, 255, 255, 192], -64),
    );
    test('returns 64', expectGet(int64, [0, 0, 0, 0, 0, 0, 0, 64], 64));

    test(
      'sets bytes to -64',
      expectSet(int64, -64, [255, 255, 255, 255, 255, 255, 255, 192]),
    );
    test('sets bytes to 64', expectSet(int64, 64, [0, 0, 0, 0, 0, 0, 0, 64]));

    test('byte length is 8', expectLength(int64, 64, 8));
  });

  group('string8', () {
    const data = 'Hello World';
    final bytes = [
      11, 72, 101, 108, 108, //
      111, 32, 87, 111,
      114, 108, 100
    ];

    test('returns "$data"', expectGet(string8, bytes, data));

    test('sets bytes to "$data"', expectSet(string8, data, bytes));

    test(
      'byte length is dynamic based on string length',
      expectLength(string8, data, 1 + data.length),
    );

    test(
      'has a minimal length of 1, indicating string length',
      expectLength(string8, '', 1),
    );
  });

  group('string16', () {
    const data = 'Hello World';
    final bytes = [
      0, 11, 72, //
      101, 108, 108,
      111, 32, 87, 111,
      114, 108, 100
    ];

    test('returns "$data"', expectGet(string16, bytes, data));

    test('sets bytes to "$data"', expectSet(string16, data, bytes));

    test(
      'byte length is dynamic based on string length',
      expectLength(string16, data, 2 + data.length),
    );

    test(
      'has a minimal length of 2, indicating string length',
      expectLength(string16, '', 2),
    );
  });

  group('string32', () {
    const data = 'Hello World';
    final bytes = [
      0, 0, 0, 11, //
      72, 101, 108, 108,
      111, 32, 87, 111,
      114, 108, 100
    ];

    test('returns "$data"', expectGet(string32, bytes, data));

    test('sets bytes to "$data"', expectSet(string32, data, bytes));

    test(
      'byte length is dynamic based on string length',
      expectLength(string32, data, 4 + data.length),
    );

    test(
      'has a minimal length of 4, indicating string length',
      expectLength(string32, '', 4),
    );
  });

  group('string64', () {
    const data = 'Hello World';
    final bytes = [
      0, 0, 0, 0, //
      0, 0, 0, 11,
      72, 101, 108, 108,
      111, 32, 87, 111,
      114, 108, 100
    ];

    test('returns "$data"', expectGet(string64, bytes, data));

    test('sets bytes to "$data"', expectSet(string64, data, bytes));

    test(
      'byte length is dynamic based on string length',
      expectLength(string64, data, 8 + data.length),
    );

    test(
      'has a minimal length of 8, indicating string length',
      expectLength(string64, '', 8),
    );
  });

  group('uint8', () {
    test('returns 8', expectGet(uint8, [8], 8));

    test('sets bytes to 8', expectSet(uint8, 8, [8]));

    test('byte length is 1', expectLength(uint8, 8, 1));
  });

  group('uint16', () {
    test('returns 16', expectGet(uint16, [0, 16], 16));

    test('sets bytes to 16', expectSet(uint16, 16, [0, 16]));

    test('byte length is 2', expectLength(uint16, 16, 2));
  });

  group('uint32', () {
    test('returns 32', expectGet(uint32, [0, 0, 0, 32], 32));

    test('sets bytes to 32', expectSet(uint32, 32, [0, 0, 0, 32]));

    test('byte length is 4', expectLength(uint32, 32, 4));
  });

  group('uint64', () {
    test('returns 64', expectGet(uint64, [0, 0, 0, 0, 0, 0, 0, 64], 64));

    test('sets bytes to 64', expectSet(uint64, 64, [0, 0, 0, 0, 0, 0, 0, 64]));

    test('byte length is 8', expectLength(uint64, 64, 8));
  });
}

void Function() expectGet<T>(PayloadType<T> type, List<int> data, T value) {
  return () {
    return expect(
      type.get(ByteData.view(Uint8List.fromList(data).buffer), 0),
      equals(value),
    );
  };
}

void Function() expectSet<T>(
  PayloadType<T> type,
  T value,
  List<int> expectedOutput,
) {
  return () {
    final setData = ByteData(expectedOutput.length);
    type.set(value, setData, 0);

    return expect(
      setData.buffer.asUint8List(),
      equals(Uint8List.fromList(expectedOutput)),
    );
  };
}

void Function() expectLength<T>(
  PayloadType<T> type,
  T value,
  int expectedByteLength,
) {
  return () {
    return expect(type.length(value), equals(expectedByteLength));
  };
}
