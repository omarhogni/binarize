import 'dart:typed_data';

import 'package:binarize/binarize.dart';
import 'package:test/test.dart';

void main() {
  final writer = Payload.write()
    ..set(uint8, 5)
    ..set(uint16, 300);

  test('binarize given PayloadWriter', () {
    expect(binarize(writer), equals(Uint8List.fromList([5, 1, 44])));
  });

  final viewWriter = Payload.write()
    ..set(uint8, 5)
    ..set(uint16, 300);

  test('view binarize given PayloadWriter', () {
    final data = ByteData(viewWriter.length);
    binarizeIntoView(viewWriter, data, 0);
    expect(data.buffer.asUint8List(), equals(Uint8List.fromList([5, 1, 44])));

    final reader = Payload.read(data.buffer.asUint8List());
    expect(reader.length, equals(uint8.length(5) + uint16.length(300)));

    expect(reader.get(uint8), equals(5));
    expect(reader.get(uint16), equals(300));
  });
}
