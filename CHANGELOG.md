## 1.1.0
- feat: added `Bytes` payload type for reading and writing list of bytes.
- feat: added support for reading and writing strings with lower and higher length storage.
  - **Deprecated**: The `string` payload type is now deprecated in favor of the `string32` payload type.
- docs: improved documentation for most types.
- feat: `PayloadReader` now has a `offset` property that indicates how far it has read.

## 1.0.1

- Improved documentation, tests and internal code.

## 1.0.0+1

- Exposing the writer and reader classes.

## 1.0.0

- Initial release of Binarize.
