import 'package:binarize/binarize.dart';

void main() {
  // Write value types to the PayloadWriter.
  final writer = Payload.write()
    ..set(boolean, true)
    ..set(flags, [true, false, true])
    ..set(float32, 1.5)
    ..set(float64, 3.5)
    ..set(int8, 5)
    ..set(int16, -15)
    ..set(int32, 38)
    ..set(int64, -2050)
    ..set(string32, 'Hello world')
    ..set(uint8, 5)
    ..set(uint16, 15)
    ..set(uint32, 30)
    ..set(uint64, 60);

  // Converting the payload to a list of bytes.
  final byteList = binarize(writer);

  // Read the bytes back into values using PayloadReader.
  Payload.read(byteList)
    ..get(boolean) // Get a boolean value.
    ..get(flags) // Get a list of 8 booleans, representing flags.

    ..get(float32) // Get a float32 value as a double.
    ..get(float64) // Get a float64 value as a double.

    ..get(int8) // Get an int8 value as an int.
    ..get(int16) // Get an int16 value as an int.
    ..get(int32) // Get an int32 value as an int.
    ..get(int64) // Get an int64 value as an int.

    ..get(string32) // Get a string value.

    ..get(uint8) // Get an uint8 value as an int.
    ..get(uint16) // Get an uint16 value as an int.
    ..get(uint32) // Get an uint32 value as an int.
    ..get(uint64); // Get an uint64 value as an int.
}
