// ignore_for_file: lines_longer_than_80_chars, avoid_print

import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class Person {
  const Person(this.name, this.age);

  final String name;

  final int age;

  @override
  String toString() {
    return '$name is $age years old';
  }
}

class _Person extends PayloadType<Person> {
  const _Person();

  @override
  int length(Person value) {
    return string32.length(value.name) + int32.length(value.age);
  }

  @override
  Person get(ByteData data, int offset) {
    final reader = Payload.read(data.buffer.asUint8List(offset));
    return Person(reader.get(string32), reader.get(int32));
  }

  @override
  void set(Person value, ByteData data, int offset) {
    final writer = Payload.write()
      ..set(string32, value.name)
      ..set(int32, value.age);

    binarizeIntoView(writer, data, offset);
  }
}

const personBinarization = _Person();

class Family {
  const Family(this.members);

  final List<Person> members;

  @override
  String toString() {
    return 'Members:\n${members.map((e) => e.toString()).join('\n')}';
  }
}

class _Family extends PayloadType<Family> {
  const _Family();

  @override
  int length(Family value) {
    var sum = int32.length(value.members.length);
    for (final member in value.members) {
      sum += personBinarization.length(member);
    }
    return sum;
  }

  @override
  Family get(ByteData data, int offset) {
    final reader = Payload.read(data.buffer.asUint8List(offset));
    final memberCount = reader.get(int32);
    final members = <Person>[];
    for (var i = 0; i < memberCount; i++) {
      members.add(reader.get(personBinarization));
    }
    return Family(members);
  }

  @override
  void set(Family value, ByteData data, int offset) {
    final writer = Payload.write()..set(int32, value.members.length);

    for (final member in value.members) {
      writer.set(personBinarization, member);
    }
    binarizeIntoView(writer, data, offset);
  }
}

const familyBinarization = _Family();

void main() {
  const bob = Person('bob', 10);

  final bobWriter = Payload.write()..set(personBinarization, bob);

  final bobBytes = binarize(bobWriter);

  final bobReader = Payload.read(bobBytes);

  final decodedBob = bobReader.get(personBinarization);

  print('Before encode');
  print(bob);

  print('After encode');
  print(decodedBob);

  const family = Family([
    Person('frank', 42),
    Person('marie', 41),
    Person('bob', 10),
    Person('chris', 8),
  ]);

  final writer = Payload.write()..set(familyBinarization, family);

  final bytes = binarize(writer);

  final reader = Payload.read(bytes);

  final fromEncode = reader.get(familyBinarization);

  print('---- Family before encode ---');
  print(family);

  print('---- Family after encode ----');
  print(fromEncode);
}
