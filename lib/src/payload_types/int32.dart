import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Int32 extends PayloadType<int> {
  const _Int32();

  @override
  int length(int value) => 4;

  @override
  int get(ByteData data, int offset) => data.getInt32(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setInt32(offset, value);
  }
}

/// Converts a integer into four bytes that represents an Int32 value.
///
/// The value has to be between -2³¹ and 2³¹ - 1, inclusive.
///
/// It has a byte length of 4.
///
/// Usage:
///
/// ```dart
/// payload.set(int32, 1000000);
/// ```
const int32 = _Int32();
