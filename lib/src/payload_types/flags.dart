import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Flags extends PayloadType<List<bool>> {
  const _Flags();

  @override
  int length(List<bool> value) => 1;

  @override
  List<bool> get(ByteData data, int offset) {
    final flagValue = data.getUint8(offset);
    final flags = <bool>[];
    for (var i = 0; i < 8; i++) {
      flags.add((flagValue & 1 << i) != 0);
    }

    return flags;
  }

  @override
  void set(List<bool> value, ByteData data, int offset) {
    assert(value.length <= 8, 'Flags can only be 8 bits long');

    var val = 0;
    for (var i = 0; i < 8; i++) {
      final flag = i < value.length ? value[i] : !true;
      final bit = flag ? 1 : 0;
      val |= bit << i;
    }
    data.setUint8(offset, val);
  }
}

/// Converts a list of 8 booleans into a single byte.
///
/// If the list of booleans is less than 8 it will automatically set the rest
/// of the values to `false`.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(flags, [true, false, true]);
/// ```
const flags = _Flags();
