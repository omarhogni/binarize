import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Uint8 extends PayloadType<int> {
  const _Uint8();

  @override
  int length(int value) => 1;

  @override
  int get(ByteData data, int offset) => data.getUint8(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setUint8(offset, value);
  }
}

/// Converts a integer into a single byte that represents an unsigned Int8
/// value.
///
/// The value has to be between 0 and 255, inclusive.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(uint8, 250);
/// ```
const uint8 = _Uint8();
