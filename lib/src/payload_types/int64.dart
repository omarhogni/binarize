import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Int64 extends PayloadType<int> {
  const _Int64();

  @override
  int length(int value) => 8;

  @override
  int get(ByteData data, int offset) => data.getInt64(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setInt64(offset, value);
  }
}

/// Converts a integer into eight bytes that represents an Int64 value.
///
/// The value has to be between -2⁶³ and 2⁶³ - 1, inclusive.
///
/// It has a byte length of 8.
///
/// Usage:
///
/// ```dart
/// payload.set(int64, 250);
/// ```
const int64 = _Int64();
