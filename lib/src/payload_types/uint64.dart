import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Uint64 extends PayloadType<int> {
  const _Uint64();

  @override
  int length(int value) => 8;

  @override
  int get(ByteData data, int offset) => data.getUint64(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setUint64(offset, value);
  }
}

/// Converts a integer into eight bytes that represents an unsigned Int64 value.
///
/// The value has to be between 0 and 2⁶⁴ - 1, inclusive.
///
/// It has a byte length of 8.
///
/// Usage:
///
/// ```dart
/// payload.set(uint64, 250);
/// ```
const uint64 = _Uint64();
