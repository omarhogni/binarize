import 'dart:typed_data';

/// {@template payload_type}
/// The base class for any payload type. It is capable of reading and writing
/// values to a `PayloadWriter` or `PayloadReader`.
///
/// Each [PayloadType] handles it's own packing and unpacking of bytes.
/// {@endtemplate}
abstract class PayloadType<T> {
  /// {@macro payload_type}
  const PayloadType();

  /// Returns the byte length of the [PayloadType] with the given [value].
  ///
  /// Is used to calculate the next offset when setting and getting data.
  int length(T value);

  /// Retrieve the value from the [data] at the given [offset].
  T get(ByteData data, int offset);

  /// Set the [value] in the [data] at the given [offset].
  void set(T value, ByteData data, int offset);
}
