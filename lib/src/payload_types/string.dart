import 'dart:convert';
import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _String extends PayloadType<String> {
  const _String(this._lengthType);

  final PayloadType<int> _lengthType;

  @override
  int length(String value) {
    final length = utf8.encode(value).length;
    return length + _lengthType.length(length);
  }

  @override
  String get(ByteData data, int offset) {
    final length = _lengthType.get(data, offset);
    final payload = Payload.read(
      data.buffer.asUint8List(offset + _lengthType.length(length), length),
    );
    return utf8.decode(payload.get(Bytes(length)));
  }

  @override
  void set(String value, ByteData data, int offset) {
    final encoded = utf8.encode(value);
    final bytes = binarize(
      Payload.write()
        ..set(_lengthType, encoded.length)
        ..set(Bytes(encoded.length), encoded),
    );

    for (var i = 0; i < bytes.length; i++) {
      data.setUint8(offset + i, bytes[i]);
    }
  }
}

/// Converts a string into an encoded UTF-8 list.
///
/// The value can not be longer than 2³² - 1.
///
/// It has a byte length of `4 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string, 'Hello world');
/// ```

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint8. Therefore it has a byte length of
/// `1 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string8, 'Hello world');
/// ```
const string8 = _String(uint8);

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint16. Therefore it has a byte length of
/// `2 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string16, 'Hello world');
/// ```
const string16 = _String(uint16);

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint8. Therefore it has a byte length of
/// `4 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string32, 'Hello world');
/// ```
const string32 = _String(uint32);

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint8. Therefore it has a byte length of
/// `4 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string32, 'Hello world');
/// ```
@Deprecated('Use string32 instead')
const string = string32;

/// Converts a string into an encoded UTF-8 list.
///
/// The length will be stored as a uint64. Therefore it has a byte length of
/// `8 + len(value)`.
///
/// Usage:
///
/// ```dart
/// payload.set(string64, 'Hello world');
/// ```
const string64 = _String(uint64);
