import 'dart:typed_data';

import 'package:binarize/binarize.dart';

/// {@template bytes}
/// Reads and write a list of bytes.
/// {@endtemplate}
class Bytes extends PayloadType<List<int>> {
  /// {@macro bytes}
  const Bytes(this.byteLength);

  /// The length of the byte list.
  final int byteLength;

  @override
  int length(List<int> value) => byteLength;

  @override
  List<int> get(ByteData data, int offset) {
    return data.buffer.asUint8List(offset, byteLength);
  }

  @override
  void set(List<int> value, ByteData data, int offset) {
    for (var i = 0; i < byteLength; i++) {
      data.setUint8(offset + i, value[i]);
    }
  }
}
