import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Int8 extends PayloadType<int> {
  const _Int8();

  @override
  int length(int value) => 1;

  @override
  int get(ByteData data, int offset) => data.getInt8(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setInt8(offset, value);
  }
}

/// Converts an integer into a single byte that represents an Int8 value.
///
/// The value has to be between -128 and 127, inclusive.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(int8, 100);
/// ```
const int8 = _Int8();
