import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Boolean extends PayloadType<bool> {
  const _Boolean();

  @override
  int length(bool value) => 1;

  @override
  bool get(ByteData data, int offset) => data.getUint8(offset) == 1;

  @override
  void set(bool value, ByteData data, int offset) {
    data.setUint8(offset, value ? 1 : 0);
  }
}

/// Converts a boolean into a single bit.
///
/// It has a byte length of 1.
///
/// Usage:
///
/// ```dart
/// payload.set(boolean, [true, false, true]);
/// ```
const boolean = _Boolean();
