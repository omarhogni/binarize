import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Int16 extends PayloadType<int> {
  const _Int16();

  @override
  int length(int value) => 2;

  @override
  int get(ByteData data, int offset) => data.getInt16(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setInt16(offset, value);
  }
}

/// Converts a integer into two bytes that represents an Int16 value.
///
/// The value has to be between -2¹⁵ and 2¹⁵ - 1, inclusive.
///
/// It has a byte length of 2.
///
/// Usage:
///
/// ```dart
/// payload.set(int16, 10000);
/// ```
const int16 = _Int16();
