import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Uint16 extends PayloadType<int> {
  const _Uint16();

  @override
  int length(int value) => 2;

  @override
  int get(ByteData data, int offset) => data.getUint16(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setUint16(offset, value);
  }
}

/// Converts a integer into two bytes that represents an unsigned Int16 value.
///
/// The value has to be between 0 and 2¹⁶ - 1, inclusive.
///
/// It has a byte length of 2.
///
/// Usage:
///
/// ```dart
/// payload.set(uint16, 250);
/// ```
const uint16 = _Uint16();
