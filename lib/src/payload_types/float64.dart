import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Float64 extends PayloadType<double> {
  const _Float64();

  @override
  int length(double value) => 8;

  @override
  double get(ByteData data, int offset) => data.getFloat64(offset);

  @override
  void set(double value, ByteData data, int offset) {
    data.setFloat64(offset, value);
  }
}

/// Converts a double into eight bytes that represents a Float64 value.
///
/// It has a byte size of 8.
///
/// Usage:
///
/// ```dart
/// payload.set(float64, 1.5);
/// ```
const float64 = _Float64();
