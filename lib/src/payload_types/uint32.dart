import 'dart:typed_data';

import 'package:binarize/binarize.dart';

class _Uint32 extends PayloadType<int> {
  const _Uint32();

  @override
  int length(int value) => 4;

  @override
  int get(ByteData data, int offset) => data.getUint32(offset);

  @override
  void set(int value, ByteData data, int offset) {
    data.setUint32(offset, value);
  }
}

/// Converts a integer into four bytes that represents an unsigned Int32 value.
///
/// The value has to be between 0 and 2³² - 1, inclusive.
///
/// It has a byte length of 4.
///
/// Usage:
///
/// ```dart
/// payload.set(uint32, 250);
/// ```
const uint32 = _Uint32();
