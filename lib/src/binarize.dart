part of binarize;

/// Convert a [PayloadWriter] into a [Uint8List] of bytes.
Uint8List binarize(PayloadWriter payload) {
  final length = payload.length;

  final uint8List = Uint8List(length);
  final byteData = ByteData.view(uint8List.buffer);

  var offset = 0;
  for (final data in payload._data) {
    final dynamic value = data.value;

    data.type.set(value, byteData, offset);
    offset += data.type.length(value);
  }

  return uint8List;
}

/// Inject a [PayloadWriter] into a [ByteData] object.
void binarizeIntoView(PayloadWriter payload, ByteData view, int offsetInBytes) {
  final length = payload.length;

  assert(
    view.lengthInBytes - view.offsetInBytes >= length,
    'ByteData view cannot contain writer data',
  );
  var offset = offsetInBytes;
  for (final data in payload._data) {
    final dynamic value = data.value;
    data.type.set(value, view, offset);
    offset += data.type.length(value);
  }
}
