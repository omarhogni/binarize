// ignore_for_file: prefer_constructors_over_static_methods

part of binarize;

/// {@template payload}
/// Base class for the [PayloadWriter] and [PayloadReader].
/// {@endtemplate}
abstract class Payload {
  /// Returns the length of the payload in bytes.
  int get length;

  /// Create a writeable payload.
  static PayloadWriter write() {
    return PayloadWriter._();
  }

  /// Create a readable payload.
  static PayloadReader read(Iterable<int> data) {
    return PayloadReader._(Uint8List.fromList(data.toList()));
  }
}

/// {@template payload_writer}
/// A writeable payload. It is used by [binarize] to create a binary ]
/// representation of a payload.
///
/// It can be created by the [Payload.write] method.
/// {@endtemplate}
class PayloadWriter extends Payload {
  PayloadWriter._();

  /// The data that is being written. It is not yet binarized.
  final _data = <_PayloadData<dynamic>>[];

  @override
  int get length {
    return _data.fold<int>(
      0,
      (v, d) => v + (d.type.length(d.value)),
    );
  }

  /// Set the given [value] using the given [PayloadType].
  void set<T>(PayloadType<T> type, T value) {
    _data.add(_PayloadData<T>(type, value));
  }
}

/// {@template payload_reader}
/// A readable payload. It is capable of reading values from a binary
/// representation into their corresponding [PayloadType]s.
///
/// It can be created by the [Payload.read] method.
/// {@endtemplate}
class PayloadReader extends Payload {
  PayloadReader._(Uint8List payload)
      : _byteData = ByteData.view(payload.buffer);

  /// The underlying [ByteData] used to read the payload.
  final ByteData _byteData;

  /// The current read offset.
  int get offset => _offset;
  var _offset = 0;

  @override
  int get length => _byteData.lengthInBytes;

  /// Get the next value by using the given [PayloadType].
  T get<T>(PayloadType<T> type) {
    final value = type.get(_byteData, _offset);
    _offset += type.length(value);
    return value;
  }
}

class _PayloadData<T> {
  const _PayloadData(this.type, this.value);

  final PayloadType<T> type;

  final T value;
}
