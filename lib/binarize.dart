/// Binarize allows for a more streamlined and extendable binary creation
/// experience.
library binarize;

import 'dart:typed_data';
import 'package:binarize/binarize.dart';

export 'src/payload_types/payload_types.dart';

part 'src/payload.dart';
part 'src/binarize.dart';
